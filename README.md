Just download this script, make it executable, and execute it.

You can even put it on your path in order to execute `laccess program_name` from everywhere.

----

![screenshot laccess](https://gitlab.com/sodimel/laccess/uploads/3c727e04b773e08bccfc372ae396eaeb/image.png)
